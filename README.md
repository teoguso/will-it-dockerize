# Will it dockerize?

Presentation given at Python Pizza Berlin, Feb 23rd 2019.


### Try it yourself

- App
```
docker run -p 8050:8050 teoguso/python-pizza:stable
```

- Slides
```
docker run -p 8000:8000 teoguso/python-pizza:slides
```

(everything runs at localhost, accessing the corresponding
port)


### Further reading

1. [An introduction to Docker](https://acadgild.com/blog/what-is-docker-container-an-introduction)
1. [Learn enough Docker to be useful](https://towardsdatascience.com/learn-enough-docker-to-be-useful-b7ba70caeb4b)
1. [15 Docker commands you should know](https://towardsdatascience.com/15-docker-commands-you-should-know-970ea5203421)
1. [Learn Docker in 12 Minutes (Video)](https://youtu.be/YFl2mCHdv24)
1. [Jupyter Docker Stacks](https://github.com/jupyter/docker-stacks)