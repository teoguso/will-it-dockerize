FROM continuumio/miniconda3:latest

# Conda
WORKDIR /app
ENV CONDAENV python-pizza-docker

# Install myapp requirements
COPY environment.yml ./environment.yml
RUN conda update -n base -c defaults conda \
    && conda config --add channels conda-forge \
    && conda env create -n $CONDAENV -f environment.yml \
    && rm -rf /opt/conda/pkgs/* \
    && conda clean -y -all \
    && echo "conda activate" $CONDAENV >> ~/.bashrc \
    && rm environment.yml

# Install myapp
COPY slides/slides.ipynb .
COPY slides/*.png ./

# activate the myapp environment
ENV PATH /opt/conda/envs/$CONDAENV/bin:$PATH

# Make ports available to the world outside this container
# 8888: Jupyter
# 4040: whatevz (Available for whatever is needed)
# 8787: Dask dashboard
# 8050: Dash
# 22: SSH
EXPOSE 80 8000
CMD ["jupyter", "nbconvert", "slides.ipynb", "--to", "slides", "--post", "serve", "--ServePostProcessor.ip=", "0.0.0.0"]
