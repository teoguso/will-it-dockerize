from datetime import datetime

import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
import numpy as np
import pandas as pd
import plotly


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash('myapp', external_stylesheets=external_stylesheets)

start = datetime.now()
times = [start]
tstamps = [start.timestamp()]
tstamps_fuzzy = [start.timestamp() + 5*(np.random.rand() - 0.5)]

def serve_layout():
    return html.Div(
        className='container',
        children=[
            html.H3('Date: {}'.format(datetime.now().date().isoformat())),
            html.H1(id='live-update-text'),
            html.H2(id='time-from-pizza'),
            html.Div(
                [
                    html.H2(id='live-update-ts'),
                ],
                className='row',
            ),
            dcc.Graph(id='live-update-graph'),
            dcc.Interval(
                id='interval-component',
                interval=1 * 1000,  # in milliseconds
                n_intervals=0,
            ),
        ],
    )


app.layout = serve_layout


@app.callback(Output('time-from-pizza', 'children'),
              [Input('interval-component', 'n_intervals')])
def update_time(n):
    time = datetime.now()
    countdown = datetime.fromisoformat('2019-02-23T12:45') - time
    days = countdown.days
    hours = countdown.seconds // 3600
    minutes = (countdown.seconds // 60 - hours * 60)
    seconds = (countdown.seconds -  hours * 3600 - minutes * 60)
    plural = lambda d: 's' if d > 1 else ''
    return html.H2(
        'Time to Pizza: '
        f'{days} day'+'{} '.format(plural(days)) +
        f'{hours} hours, '
        f'{minutes} minutes, '
        f'and {seconds} seconds. '
    )


@app.callback(Output('live-update-graph', 'figure'),
              [Input('interval-component', 'n_intervals')])
def update_graph(n):
    dt = datetime.now()
    times.append(dt)
    tstamps.append(dt.timestamp())
    tstamps_fuzzy.append(
        dt.timestamp() + 5*(np.random.rand() - 0.5) + 50*(np.random.rand() - 0.5) * int(
            np.random.rand() > .95
        )
    )
    fig = plotly.tools.make_subplots(1, 1)
    fig['layout']['margin'] = {
        'l': 40, 'r': 10, 'b': 40, 't': 10
    }
    fig['layout']['xaxis'] = dict(title='Time')
    fig['layout']['yaxis'] = dict(title='Seconds')
    fig.append_trace(
        {
            'x': pd.to_datetime(times),
            'y': np.array(tstamps_fuzzy) - start.timestamp(),
            'name': 'Seconds',
            # 'mode': 'lines+markers',
            'type': 'scatter'
        },
        1,
        1,
    )
    return fig


@app.callback(Output('live-update-ts', 'children'),
              [Input('interval-component', 'n_intervals')])
def update_ts(n):
    dt = datetime.now()
    return html.H4([
        '{} seconds'.format(int(dt.timestamp())),
        ' since the Unix ',
        html.A(
            'Epoch',
            href='https://en.wikipedia.org/wiki/Epoch_(computing)',
            target='_blank',
        ),
    ]),


@app.callback(Output('live-update-text', 'children'),
              [Input('interval-component', 'n_intervals')])
def update_time(n):
    time = datetime.now().time()
    return ['The time is {}'.format(time.strftime('%H:%M:%S'))]


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0')
